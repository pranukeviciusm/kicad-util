package dk.dren.kicad.cmd;

import dk.dren.kicad.pcb.Board;
import dk.dren.kicad.pcb.KicadPcbSerializer;
import lombok.Getter;
import picocli.CommandLine;

import java.io.File;
import java.util.concurrent.Callable;

@Getter
@CommandLine.Command(name = "pcb", description = "Operates on a .kicad_pcb file",
        subcommands = {
            AnchoredCloneCmd.class, ArrayCloneCmd.class, CleanCloneCmd.class,
            TextCmd.class, PanelStitching.class, Roundover.class
            })
public class PCBCmd implements Callable<Integer> {

    @CommandLine.Option(names = {"-f", "--file"},
            required = true,
            description = "The .kicad_pcb file to work on")
    private File pcbFile;

    private Board board;

    @Override
    public Integer call() throws Exception {
        board = KicadPcbSerializer.read(pcbFile);
        return 0;
    }

}
