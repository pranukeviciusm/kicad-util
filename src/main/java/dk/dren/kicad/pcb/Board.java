package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.ModuleReference;
import dk.dren.kicad.primitive.Point;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;
import org.decimal4j.immutable.Decimal6f;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public class Board implements Node {
    public static final String NAME = "kicad_pcb";
    @Delegate
    private final RawNode rawNode;
    private Map<ModuleReference, Module> moduleByReference;
    @Getter
    @Setter
    private File file;

    @Override
    public String toString() {
        return "Board with " + nets().count()+" nets and "+ modules().count()+" modules";
    }

    public Stream<Module> modules() {
        return this.streamChildNodesByName(Module.NAME).map(n->(Module)n);
    }

    public Stream<Node> nets() {
        return this.streamChildNodesByName("net");
    }

    public Stream<Zone> zones() {
        return this.streamChildNodesByName(Zone.NAME).map(n->(Zone)n);
    }

    public Stream<RawNode> vias() {
        return streamChildNodesByName("via").map(n->(RawNode)n);
    }

    public Stream<RawNode> segments() {
        return streamChildNodesByName("segment").map(n->(RawNode)n);
    }

    public Module getModuleWithReference(String reference) {
        return getModuleWithReference(new ModuleReference(reference));
    }

    public Module getModuleWithReference(ModuleReference reference) {
        if (moduleByReference == null) {
            moduleByReference = modules().collect(Collectors.toMap(Module::getReference, m->m));
        }
        return moduleByReference.get(reference);
    }

    public Stream<LineNode> graphicLines() {
        return streamChildNodesByName("gr_line").map(n->(LineNode)n);
    }

    public Stream<LineNode> lines(String layerName) {
        return graphicLines().filter(l->l.getLayerName().equals(layerName));
    }

    public Stream<LineNode> edgeCuts() {
        return lines(LineNode.EDGE_CUTS);
    }

    public Stream<LineNode> eco1UserLines() {
        return lines(LineNode.ECO1_USER);
    }

    /**
     * Finds the edge cut line connecting to a point
     * If no exact point can be found, then the closest line-start-point within 0.5 mm will be picked.
     *
     * @param point The point to find a starting point at
     * @return The line and the point at the far end of that line and the distance from the point
     */
    public LineNodeIntersection findEdgeCutLineConnectingTo(Point point) {
        return findLineNodeIntersection(point, LineNode.EDGE_CUTS);
    }

    private LineNodeIntersection findLineNodeIntersection(Point point, String layerName) {
        List<LineNode> cuts = lines(layerName).collect(Collectors.toList());

        // First try exact match, which is fast and easy:
        for (LineNode edgeCut : cuts) {
            if (edgeCut.getEnds().getEnd().equals(point)) {
                return new LineNodeIntersection(edgeCut.getEnds().getStart(), edgeCut);
            }
            if (edgeCut.getEnds().getStart().equals(point)) {
                return new LineNodeIntersection(edgeCut.getEnds().getEnd(), edgeCut);
            }
        }

        // Then try to find the closest point, which can be quite expensive
        LineNodeIntersection leastFudge = null;
        for (LineNode edgeCut : cuts) {
            Decimal6f endFudge = Vector.between(edgeCut.getEnds().getEnd(), point).length();
            if (leastFudge == null || endFudge.isLessThan(leastFudge.getFudge())) {
                leastFudge = new LineNodeIntersection(edgeCut.getEnds().getStart(), edgeCut, endFudge);
            }
            Decimal6f startFudge = Vector.between(edgeCut.getEnds().getStart(), point).length();
            if (leastFudge == null || startFudge.isLessThanOrEqualTo(leastFudge.getFudge())) {
                leastFudge = new LineNodeIntersection(edgeCut.getEnds().getEnd(), edgeCut, startFudge);
            }
        }

        if (leastFudge.getFudge().isLessThanOrEqualTo(Decimal6f.valueOf(0.5))) {
            return leastFudge;
        } else {
            return null;
        }
    }

    public LineNodeIntersection findEdgeCutIntersecting(Point outside, Point inside) {
        final Line middle = new Line(outside, inside);

        for (LineNode edgeCut : edgeCuts().collect(Collectors.toList())) {
            Point i = edgeCut.getEnds().intersection(middle);
            if (i != null) {
                return new LineNodeIntersection(i, edgeCut);
            }
        }

        return null;
    }
}
